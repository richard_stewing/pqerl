%uses the pq-method to solve quadratic equations
-module (pq).
-export ([pq/2]).

-spec(pq(number(), number() )->tuple() ).
%%@doc only exported function, gets the q and p values as its arguments to begin with semanticle calculation of the x values where the y value is 0 
pq(P, Q)->
	D = (P/2)*(P/2)-Q,
	Ans = ev(D, P),	
	Ans.

-spec(ev(number(), number() )->tuple() ).
ev(D, P)->
if
	D<0 ->
		{{"No Answere"}, D};
	D==0 ->
		{{-1*(P/2)}, D};
	D>0 ->
		{{(-1*(P/2)-math:sqrt(D)), (-1*(P/2)+math:sqrt(D))}, D}

end.
